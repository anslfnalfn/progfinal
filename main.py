import csv

class Student:
    def __init__(self, student__id, student__name, student__midterm, student__final):
        self.student__id = int(student__id)
        self.student__name = student__name
        self.student__midterm = float(student__midterm)
        self.student__final = float(student__final)

    def student__total(self):
        self.student__total = self.student__final + self.student__midterm

    def student__status(self):
        if self.student__total > 50:
            self.student__status = 'Pass'
            return
        self.student__status = 'Fail'

    def create__output__obj(self):
        return f"{self.student__id},{self.student__name},{self.student__total},{ self.student__status}"

def summary(): 
    students = []
    marks = []
    with open('input_file.csv', 'r') as input:
        for l, i in enumerate(input):
            if l != 0:       
                student__id, student__name, student__midterm, student__final = i.split(',')
                temp__student = Student(student__id, student__name, student__midterm, student__final)
                temp__student.student__total()
                temp__student.student__status()
                students.append(temp__student.create__output__obj())
                marks.append(float(temp__student.create__output__obj().split(',')[2]))

    L = [float(n) for n in marks if n]
    avg = sum(L)/len(L) if L else '-'
    avg = round(avg, 2)

    highest__score = f'{max(marks)}'
    for l, i in enumerate(students):
        if highest__score in i:
            highest__name = i.split(',')[1]
        
    print(f"# of students: {len(students)}")
    print()
    print(f"average mark: {avg}")
    print()
    print(f"student with highest mark: {highest__name}, {highest__score}")

    with open('output_file.csv', 'w', newline = '') as data:
        writer = csv.writer(data, delimiter=',')
        writer.writerow('Id,name,total,status'.split(','))
        for i in range (len(students)):
            writer.writerow(students[i].split(','))

summary()